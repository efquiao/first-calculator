const addBtn = document.querySelector('#add');
addBtn.addEventListener('click', addInputs);

const subBtn = document.querySelector('#sub');
subBtn.addEventListener('click', subInputs);

const mulBtn = document.querySelector('#mul');
mulBtn.addEventListener('click', mulInputs);

const divBtn = document.querySelector('#div');
divBtn.addEventListener('click', divInputs);

function addInputs(){ 
    const first = firstValue();
    const second = secondValue();
    const key = inputCheck();
  
    if(key ==1){
       clearTextBox();
    }
    if(key==0){
    const result = Number(first)+Number(second); 
    document.querySelector('#result').innerHTML = result.toFixed(2);
    }
}

function subInputs(){ 
    const first = firstValue();
    const second = secondValue();
    const key = inputCheck();
  
    if(key ==1){
       clearTextBox();
    }
    if(key==0){
    const result = Number(first)-Number(second); 
    document.querySelector('#result').innerHTML = result.toFixed(2);
    }
}

function mulInputs(){ 
    const first = firstValue();
    const second = secondValue();
    const key = inputCheck();
  
    if(key ==1){
       clearTextBox();
    }
    if(key==0){
    const result = Number(first)*Number(second); 
    document.querySelector('#result').innerHTML = result.toFixed(2);
    }
}

function divInputs(){ 
    const first = firstValue();
    const second = secondValue();
    const key = inputCheck();
  
    if(key ==1){
       clearTextBox();
    }
    if(key==0){
    const result = Number(first)/Number(second); 
    document.querySelector('#result').innerHTML = result.toFixed(2);
    }
}

function firstValue() {
    let firstIn = document.querySelector('#firstInput').value;
    return (firstIn);
}

function secondValue() {
    let secondIn = document.querySelector('#secondInput').value;
    return (secondIn);
}

function inputCheck(){  
    let incta = 0
    let inctb = 0
     incta = document.getElementById("firstInput").value.length
     inctb = document.getElementById("secondInput").value.length
    let totint = incta * inctb
    if (totint == 0){
        let key = 1
        return(key)
    }else{
        let key =0;
        return(key);
    }   
  
}

function clearTextBox(){
    let resulta=  "";
    document.querySelector('#result').innerHTML = resulta;
   
}
