const addBtn = document.querySelector('#add'); // const - declaring a variable, addBtn - variable name, document.querySelector -searching a source html, '#add' - id
addBtn.addEventListener('click', addInputs); //addBtn - declared variable, addEveListener

//const subBtn = document.querySelector('#sub');
//subBtn.addEventListener('click', subInputs);

//const mulBtn = document.querySelector('#mul');
//mulBtn.addEventListener('click', mulInputs);

//const divBtn = document.querySelector('#div');
//divBtn.addEventListener('click', divInputs);

function getValues(){

    const inputa = document.querySelector('#firstInput').value;
    const inputb = document.querySelector('#secondInput').value;

    return {
      //  first: inputa,
      //  second: inputb,
        firstValue: Number(inputa),
        secondValue: Number(inputb),
        isValid: inputa !== '' && inputb !== '',

    }
}


function addInputs () { 

    const values = getValues();    

    if (values.isValid) {
    const result = (values.firstValue + values.secondValue);
    document.querySelector('#result').innerHTML = result.toFixed(2)
    }
}

 