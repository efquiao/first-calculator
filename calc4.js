const addBtn = document.querySelector('#add');
addBtn.addEventListener('click', executeadd);

const subBtn = document.querySelector('#sub');
subBtn.addEventListener('click', subInputs);

const mulBtn = document.querySelector('#mul');
mulBtn.addEventListener('click', mulInputs);

const divBtn = document.querySelector('#div');
divBtn.addEventListener('click', divInputs);

function getValues(){

    const inputa = document.querySelector('#firstInput').value;
    const inputb = document.querySelector('#secondInput').value;

    return{
    container1: Number(inputa),
    container2: Number(inputb),
    isValid: inputa !== '' && inputb !== '',
    }
}

function executeadd(){
const values = getValues();
if(values.isValid){
    const result = values.container1 + values.container2;
    document.querySelector('#result').innerHTML = result.toFixed(2);
    }
}

function subInputs(){
    const values = getValues();
    if(values.isValid){
        const result = values.container1 - values.container2;
        document.querySelector('#result').innerHTML = result.toFixed(2);
        }
    }

function mulInputs(){
    const values = getValues();
    if(values.isValid){
        const result = values.container1 * values.container2;
        document.querySelector('#result').innerHTML = result.toFixed(2);
        }
    }

    function divInputs(){
        const values = getValues();
        if(values.isValid){
            const result = values.container1 / values.container2;
            document.querySelector('#result').innerHTML = result.toFixed(2);
            }
        }


